/**
 * This is the script that is always running in the background of the extension, regardless of
 * whether or not the popup/window is open.
 *
 */

var screenWidth = window.screen.availWidth;
var screenHeight = window.screen.availHeight;
var halfWidth = (screenWidth / 2) + 5;
var halfHeight = (screenHeight / 2) + 5;

var positionOptions = {
    l: { top: 0, left: 0, height: screenHeight, width: halfWidth },
    r: { top: 0, left: halfWidth, height: screenHeight, width: halfWidth },
    tl: { top: 0, left: 0, height: halfHeight, width: halfWidth },
    tr: { top: 0, left: halfWidth, height: halfHeight, width: halfWidth },
    bl: { top: screenHeight, left: 0, height: halfHeight, width: halfWidth },
    br: { top: screenHeight, left: halfWidth, height: halfHeight, width: halfWidth }
}

function openNew(position, url) {
    var opts = positionOptions[position];

    var newWin = window.open(url, '', '' +
        'location=no,' +
        'status=no,' +
        'titlebar=no,' +
        'toolbar=no,' +
        'menubar=no,' +
        'left=' + opts.left + ',' +
        'top=' + opts.top + ',' +
        'height=' + opts.height + ',' +
        'width=' + opts.width
    );
}

var menuItemClicked = function(info, tab) {
    openNew(info.menuItemId, info.pageUrl);
};

chrome.contextMenus.create({ title: 'Pop Left', id: 'l', onclick: menuItemClicked });
chrome.contextMenus.create({ title: 'Pop Right', id: 'r', onclick: menuItemClicked });
chrome.contextMenus.create({ title: 'Pop Top Left', id: 'tl', onclick: menuItemClicked });
chrome.contextMenus.create({ title: 'Pop Top Right', id: 'tr', onclick: menuItemClicked });
chrome.contextMenus.create({ title: 'Pop Bottom Left', id: 'bl', onclick: menuItemClicked });
chrome.contextMenus.create({ title: 'Pop Bottom Right', id: 'br', onclick: menuItemClicked });
