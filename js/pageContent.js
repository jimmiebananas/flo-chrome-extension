/**
 *
 * This is the script that executed on every single page while the extension is running.
 *
 */

var playerClass = '.oo-player-container';

function isolateFloVideo() {
    if (window.opener) {
        $('body').css('background-color', '#272a2b');
        $(playerClass).css('position', 'fixed');
        $(playerClass).css('width', '100%');

        $(playerClass).insertAfter('flo-app');
        $('flo-app').remove();
    }
}

setTimeout(isolateFloVideo, 3000);
